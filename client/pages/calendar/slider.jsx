'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
import Slider from 'react-slick';



class TimeSlideElement extends React.Component {

    render() {
        const settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };
        return (
            <Slider {...settings}>
                <div className="time"><span className="active">12:30</span></div>
                <div className="time"><span>14:00</span></div>
                <div className="time"><span>18:00</span></div>
                <div className="time"><span>20:00</span></div>
                <div className="time"><span>21:30</span></div>
        </Slider>
        )
    }
}


module.exports = TimeSlideElement;