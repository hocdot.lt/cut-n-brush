'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
const flatpickr =  require('flatpickr');
const France = require("flatpickr/dist/l10n/fr.js").fr;

class CalendarElement extends React.Component {
    constructor(props) {
        super(props);
        this.input = {};
    }
    componentDidMount(){
        flatpickr("#calendar-select",{inline: true,minDate: "today",weekNumbers: true,locale: France});
        console.log(1);
    }

    render() {
        return (
            <main>
                <input type="text" id="calendar-select" />
            </main>

        )
    }
}


module.exports = CalendarElement;