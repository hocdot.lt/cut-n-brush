/* global window */
'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
//const Config = require('../../config');
import BurgerMenu from 'react-burger-menu';

class SearchResultCPage {
    static blastoff() {
        const Menu = BurgerMenu['pushRotate'];
        this.mainElement = ReactDOM.render(
            <div className="search-result-page inner-app" id="inner-app">
                <Menu  id={"pushRotate"} right outerContainerId={"inner-app"} pageWrapId={'menu-wrap'} width={ '320px' }>
                    <h2>
                        <img src="/public/media/avatar.jpg"/>
                        <p>Callara Vincent</p>
                    </h2>
                    <a id="home" className="menu-item" href="/">ACCUEIL</a>
                    <a id="notification" className="menu-item" href="/about">Notification</a>
                    <a id="profile" className="menu-item" href="/about">Profile</a>
                    <a id="images" className="menu-item" href="/about">Images</a>
                    <a id="calendar-setting" className="menu-item" href="/about">Calendar setting</a>
                    <a id="orders" className="menu-item" href="/about">Orders</a>
                    <a id="reviews" className="menu-item" href="/about">Revies</a>
                    <a id="logout" className="menu-item" href="/logout">Logout</a>
                </Menu>
                <div id="menu-wrap">
                    <div className="header-page black">
                        <a className="back-page" href="#"></a>
                        <h3 className="page-title">choisir son coiffeur</h3>
                    </div>
                    <div className="menu-second">
                        <ul>
                            <li><a href="#">Historique coiffeur</a></li>
                            <li><a href="#">Coiffeur du mois</a></li>
                            <li className="search-input">
                                <form action="" method="post">
                                    <input type="text" placeholder="recherche coiffeur" className="search-input"/>
                                    <button type="submit"></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div className="inner-page">
                        <div className="logo"><img src="/public/media/logo-inner.png"/></div>
                        <div className="content">
                            <div className="total-result"><h3>6 coiffeurs trouvés selon vos critères</h3></div>
                            <div className="list-result">
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-5-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                </span>
                                            <span className="number">5 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-4-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/no-star.png"/>
                                                </span>
                                            <span className="number">4 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-5-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                </span>
                                            <span className="number">5 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-5-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                </span>
                                            <span className="number">5 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-5-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                </span>
                                            <span className="number">5 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-result">
                                        <h3 className="name">Callara Vincent</h3>
                                        <p className="service">Coiffeur homme/femme</p>
                                        <p className="rating star-5-5">
                                                <span className="star">
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                    <img src="/public/media/star.png"/>
                                                </span>
                                            <span className="number">5 / 5</span>
                                        </p>
                                        <p className="description">Exceptionnel, rapide, sympa, top !</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="footer-menu">
                        <ul className="footer-nav">
                            <li className="none-icon"><a href="#"><p></p><span>tendances</span></a></li>
                            <li><a href="#"><p><img src="/public/media/heart-icon.png" /></p><span>FAVORIS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/scissor-icon.png"/></p><span>COIFFEURS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/clock-icon.png"/></p><span>RENDEZ-VOUS</span></a></li>
                            <li className="profile"><a href="#"><p><img src="/public/media/avatar.jpg"/></p><span>PROFIL</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>,
            window.document.getElementById('app-mount')
        );
    }
}


module.exports = SearchResultCPage;


/* $lab:coverage:off$ */
if (!module.parent) {
    window.page = SearchResultCPage;
    SearchResultCPage.blastoff();
}
/* $lab:coverage:on$ */
