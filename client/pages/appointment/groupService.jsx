'use strict';
const React = require('react');
const Redux = require('redux');
const SelectElement = require('../../components/form/select-element.jsx');

class ServiceGroup extends React.Component {
    constructor(props) {
        super(props);
        this.input = {};
        this.state = {service_option:['Coupe courte','Coupe rasoir','Barbe'],service_option_F: ['Coupe courte','Coupe rasoir','Barbe'],service_option_H:['Coupe','Brushing','Coupe & Brushing','Chignon']};
    }
    value() {
        return {gender:this.input.gender.value(),service:this.input.service.value() };
    }
    changeOptionService(){
        console.log(this.input.gender.value());
        if( this.input.gender.value() == 'Femme'){
            this.setState({service_option:this.state.service_option_F});
        }else{
            this.setState({service_option:this.state.service_option_H});
        }
    }
    render() {
        return (
                <div className="group-service">
                    <div className="wapper-selectbox">
                        <SelectElement
                            ref={(c) => (this.input.gender = c)}
                            onChange={this.changeOptionService.bind(this)}>
                            <option value="Femme">Femme</option>
                            <option value="Homme">Homme</option>
                        </SelectElement>
                    </div>
                    <div className="wapper-selectbox">
                        <SelectElement
                            ref={(c) => (this.input.service = c)}>
                            {this.state.service_option.map((index) => (
                                <option value="{index}">{index}</option>
                            ))}
                        </SelectElement>
                    </div>
                </div>
        );
    }
}


module.exports = ServiceGroup;