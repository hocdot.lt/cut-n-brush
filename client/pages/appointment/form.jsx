'use strict';
const React = require('react');
const SelectElement = require('../../components/form/select-element.jsx');
const ServiceGroup = require('./groupService.jsx');
const Redux = require('redux');
class FormService extends React.Component {
    constructor(props) {
        super(props);
        this.input = {};
        this.state = {personnes: 1};
    }
    updatePersonnes(){
        this.setState({personnes:this.input.personnes.value()});
    }
    render() {
        let service_row = [];
        for (var i = 0; i < this.state.personnes; i++) {
            service_row.push(<ServiceGroup/>);
        }
        return (
            <form>
                <div className="service-option">
                    <h2 className="title">Services</h2>
                    <div className="wapper-selectbox">
                        <SelectElement
                            ref={(c) => (this.input.service = c)}
                            name="service">
                            <option value="Coupe courte">Coupe courte</option>
                            <option value="Coupe rasoir">Coupe rasoir</option>
                            <option value="Barbe">Barbe</option>
                        </SelectElement>
                    </div>
                </div>

                <div className="more-person">
                    <p>Partagez cette expérience avec <br/> vos ami(e)s ou membre de la famille ?</p>
                    <div className="number_personnes full-width">
                        <div className="wapper-selectbox">
                            <SelectElement
                                ref={(c) => (this.input.personnes = c)}
                                name="number_personnes"
                                onChange={this.updatePersonnes.bind(this)}>
                                <option value="1">1 personnes</option>
                                <option value="2">2 personnes</option>
                                <option value="3">3 personnes</option>
                                <option value="4">4 personnes</option>
                                <option value="5">5 personnes</option>
                                <option value="6">6 personnes</option>
                            </SelectElement>
                        </div>
                    </div>
                </div>
                {service_row}
                <div className="submitForm">
                    <button type="submit">Rechercher coiffeur(euse)</button>
                </div>
            </form>
        );
    }
}


module.exports = FormService;