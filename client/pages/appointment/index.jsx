'use strict';
const FormService = require('./form.jsx');
const React = require('react');
const ReactDOM = require('react-dom');
import BurgerMenu from 'react-burger-menu';

class AppointmentPage {
    static blastoff() {
        const Menu = BurgerMenu['pushRotate'];
        this.mainElement = ReactDOM.render(
            <div className="appointment-page inner-app" id="inner-app">
                <Menu  id={"pushRotate"} right outerContainerId={"inner-app"} pageWrapId={'menu-wrap'} width={ '320px' }>
                    <h2>
                        <img src="/public/media/avatar.jpg"/>
                        <p>Callara Vincent</p>
                    </h2>
                    <a id="home" className="menu-item" href="/">ACCUEIL</a>
                    <a id="notification" className="menu-item" href="/about">Notification</a>
                    <a id="profile" className="menu-item" href="/about">Profile</a>
                    <a id="images" className="menu-item" href="/about">Images</a>
                    <a id="calendar-setting" className="menu-item" href="/about">Calendar setting</a>
                    <a id="orders" className="menu-item" href="/about">Orders</a>
                    <a id="reviews" className="menu-item" href="/about">Revies</a>
                    <a id="logout" className="menu-item" href="/logout">Logout</a>
                </Menu>
                <div id="menu-wrap">
                    <div className="header-page black">
                        <h1 className="page-title">prise de Rendez-vous</h1>
                    </div>
                    <div className="inner-page">
                        <div className="logo"><img src="/public/media/logo-inner-white.png"/></div>
                        <div className="content">
                            <FormService />
                        </div>
                        <div className=""></div>
                    </div>
                    <div className="footer-menu">
                        <ul className="footer-nav">
                            <li class="none-icon"><a href="#"><p></p><span>tendances</span></a></li>
                            <li><a href="#"><p><img src="/public/media/heart-icon.png" /></p><span>FAVORIS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/scissor-icon.png"/></p><span>COIFFEURS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/clock-icon.png"/></p><span>RENDEZ-VOUS</span></a></li>
                            <li className="profile"><a href="#"><p><img src="/public/media/avatar.jpg"/></p><span>PROFIL</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>,
            window.document.getElementById('app-mount')
        );
    }
}


module.exports = AppointmentPage;



if (!module.parent) {
    window.page = AppointmentPage;
    AppointmentPage.blastoff();
}