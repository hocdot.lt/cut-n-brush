/* global window */
'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
//const Config = require('../../config');
import StepZilla from 'react-stepzilla';
const Step1 = require('./step1.jsx');
const Step2 = require('./step2.jsx');

class RegisterPage {
    static blastoff() {
        const steps =
            [
                {name: 'Step 1', component: <Step1 />},
                {name: 'Step 2', component: <Step2 />},
            ];
        this.mainElement = ReactDOM.render(
            <div className='step-progress'>
                <StepZilla steps={steps} />
            </div>,
            window.document.getElementById('app-mount')
        );
    }
}


module.exports = RegisterPage;


/* $lab:coverage:off$ */
if (!module.parent) {
    window.page = RegisterPage;
    RegisterPage.blastoff();
}
/* $lab:coverage:on$ */
