/* global window */
'use strict';
const ApiActions = require('../../actions/api');
const Constants = require('./constants');
const ReactRouter = require('react-router');
const SignInStore = require('./home/store');
const FacebookStore = require('./facebook/store');


class Actions {
    static sendRequest(data) {

        ApiActions.post(
            '/api/signup',
            data,
            SignInStore,
            Constants.REGISTER,
            Constants.REGISTER_RESPONSE,
            (err, response) => {
                if (!err) {
                    window.location.href = '/member/client';
                }
            }
        );
        /*
        ApiActions.post(
            '/api/facebook-login',
            data,
            FacebookStore,
            Constants.REGISTER,
            Constants.REGISTER_RESPONSE,
            (err, response) => {
                if (!err) {
                    //window.location.href = '/';
                }else{
                    if(response.message == 'facbook_login'){
                       // window.location.href = '/login';
                    }
                }
            }
        );
        */
    }
    static sendRequestFacebook(data){
        ApiActions.post(
            '/api/facebook-login',
            data,
            FacebookStore,
            Constants.REGISTER,
            Constants.REGISTER_RESPONSE,
            (err, response) => {
                if (!err) {
                    window.location.href = '/member/client';
                }
            }
        );
    }
};


module.exports = Actions;