'use strict';
const React = require('react');
const ReactRouter = require('react-router');
import StepZilla from 'react-stepzilla';

const Step1 = require('./step/step1.jsx');
const Step2 = require('./step/step2.jsx');
const Step3 = require('./step/step3.jsx');
const Step4 = require('./step/step4.jsx');
const Step5 = require('./step/step5.jsx');
const Step6 = require('./step/step6.jsx');
const Step7 = require('./step/step7.jsx');
const Step8 = require('./step/step8.jsx');

const Actions = require('../actions');
const Store = require('./store');

class MultiStepPage extends React.Component {
    constructor(props) {
        super(props);
        this.input = {};
        this.state = {};
        this.sampleUser = {
            profile_email: '',
            profile_pass: '',
            profile_gender: 'Homme',
            profile_prenom: '',
            profile_etudiant: '',
            profile_nom: '',
            profile_date: '',
            profile_cheveux_1: '',
            profile_cheveux_2: '',
            profile_cheveux_3: '',
            profile_phone: '+41',
            profile_np: '',
            profile_localtion: '',
            profile_no: '',
            profile_rue: '',
            profile_code_porte: '',
            profile_animal_de_compagnie: '',
            profile_bleue: '',
            profile_blanche: '',
            profile_visiteur: '',
            profile_privee: '',
            profile_no_de_place: '',
            profile_avartar: '',
            profile_cover_image: ''
        }

    }
    getUser() {
        return this.sampleUser;
    }
    updateUser(update) {
        this.sampleUser = this.updateFunction(this.sampleUser,update);
        this.setState(this.sampleUser);
    }
    updateFunction(obj, newobj) {

            for (var prop in newobj) {
                var val = newobj[prop];
                obj[prop] = val;
            }

        return obj;
    }

    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        Actions.sendRequest({
            profile_email: this.state.profile_email,
            profile_pass: this.state.profile_pass,
            profile_gender: this.state.profile_gender,
            profile_prenom: this.state.profile_prenom,
            profile_etudiant: this.state.profile_etudiant,
            profile_nom: this.state.profile_nom,
            profile_date: this.state.profile_date,
            profile_cheveux_1: this.state.profile_cheveux_1,
            profile_cheveux_2: this.state.profile_cheveux_2,
            profile_cheveux_3: this.state.profile_cheveux_3,
            profile_phone: this.state.profile_phone,
            profile_np: this.state.profile_np,
            profile_localtion: this.state.profile_localtion,
            profile_no: this.state.profile_no,
            profile_rue: this.state.profile_rue,
            profile_code_porte: this.state.profile_code_porte,
            profile_animal_de_compagnie: this.state.profile_animal_de_compagnie,
            profile_bleue: this.state.profile_bleue,
            profile_blanche: this.state.profile_blanche,
            profile_visiteur: this.state.profile_visiteur,
            profile_privee: this.state.profile_privee,
            profile_no_de_place: this.state.profile_no_de_place,
            profile_avartar: this.state.profile_avartar,
            profile_cover_image: this.state.profile_cover_image
        });
    }

    render() {
        const steps = [
                {name: 'Step 1', component: <Step1 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}}/>},
                {name: 'Step 2', component: <Step2 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 3', component: <Step3 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 4', component: <Step4 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 5', component: <Step5 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 6', component: <Step6 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 7', component: <Step7 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} />},
                {name: 'Step 8', component: <Step8 getUser={() => (this.getUser())} updateUser={(u) => {this.updateUser(u)}} handleSubmit={(u) => {this.handleSubmit(u)}} />}
            ];
        return (
            <div className='step-progress'>
                <StepZilla steps={steps} showSteps={false} startAtStep={0}/>
            </div>
        );
    }
}
module.exports = MultiStepPage;
