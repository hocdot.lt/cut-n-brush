'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step5 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_avartar: props.getUser().profile_avartar,
            preview_avartar: '',
            profile_cover_image: props.getUser().profile_cover_image,
            preview_cover_image: '',
            profile_cheveux_1: props.getUser().profile_cheveux_1,
            profile_cheveux_2: props.getUser().profile_cheveux_2,
            profile_cheveux_3: props.getUser().profile_cheveux_3
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }


    isValidated() {
        const userInput = this._grabUserInput();
        let isDataValid = false;
        this.props.updateUser(userInput);
        isDataValid = true;
        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            notrequired: true
        }
    }

    _validationErrors(val) {
        const errMsgs = {};
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_avartar: this.state.profile_avartar,
            profile_cover_image: this.state.profile_cover_image,
            profile_cheveux_1: this.refs.profile_cheveux_1.value,
            profile_cheveux_2: this.refs.profile_cheveux_2.value,
            profile_cheveux_3: this.refs.profile_cheveux_3.value
        };
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                profile_avartar: reader.result,
                preview_avartar: reader.result
            });
        }
        reader.readAsDataURL(file)
    }

    _handleCoverImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                profile_cover_image: reader.result,
                preview_cover_image: reader.result
            });
        }

        reader.readAsDataURL(file)
    }



    render() {
        let $imagePreview = '';
        let $imageCoverPreview = '';


        if(this.state.profile_avartar){
            $imagePreview = (<img src={this.state.profile_avartar} />);
        }else{
            $imagePreview = "PHOTO";
        }
        if(this.state.profile_cover_image){
            $imageCoverPreview = (<img src={this.state.profile_cover_image} />);
        }else{
            $imageCoverPreview = "COVER IMAGE";
        }
        return (
            <div className="inner step step-5">
                <div className="list-fields">
                    <h2>Merci de compléter votre profil</h2>
                    <div className="bron_irene">
                        <div className="profile_avatar">
                            <div className="desc">
                                <h2>Bron Irène</h2>
                                <p>Ajouter une photo de profil</p>
                            </div>
                            <div className="upload_image">
                                <label htmlFor="profile_avartar">{$imagePreview}</label>
                                <input
                                    type="file"
                                    ref="profile_avartar"
                                    name="profile_avartar"
                                    id="profile_avartar"
                                    className="fileUpload"
                                    onChange={(e)=>this._handleImageChange(e)}
                                />
                            </div>
                        </div>
                        <div className="profile_cover_image">
                            <p>Cover image</p>
                            <div className="upload_cover_image">
                                <label htmlFor="profile_cover_image">{$imageCoverPreview}</label>
                                <input
                                    type="file"
                                    ref="profile_cover_image"
                                    name="profile_cover_image"
                                    id="profile_cover_image"
                                    className="fileUpload"
                                    onChange={(e)=>this._handleCoverImageChange(e)}
                                />
                            </div>
                        </div>
                    </div>
                    <h2>Type de cheveux</h2>
                    <div className="fieldGroup">
                        <div className="field">
                            <select
                                ref='profile_cheveux_1'
                                name="profile_cheveux_1"
                            >
                                <option value="court">Court</option>
                                <option value="mi-long">mi-long</option>
                                <option value="long">long</option>
                            </select>
                        </div>
                        <div className="field">
                            <select
                                name="profile_cheveux_2"
                                ref='profile_cheveux_2'
                            >
                                <option value="raide">Raide</option>
                                <option value="Ondulé">Ondulé</option>
                                <option value="Frisé">Frisé</option>
                            </select>
                        </div>
                        <div className="field">
                            <select
                                name="profile_cheveux_3"
                                ref='profile_cheveux_3'
                            >
                                <option value="fin">Fin</option>
                                <option value="Épais">Épais</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step5;