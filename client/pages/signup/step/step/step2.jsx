'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_date: props.getUser().profile_date
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            if (this.props.getUser().profile_date != userInput.profile_date) {
                this.props.updateUser(userInput);
            }

            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }

        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_dateVal: (data.profile_date != '')
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_dateValMsg: val.profile_dateVal ? '' : 'The birthday field is required'
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_date: this.refs.profile_date.value
        };
    }
    render() {
        return (
            <div className="inner step step-2">
                <div className="list-fields">
                    <h2>Quel est votre date de naissance ?</h2>
                    <p className="desc">Cette information vous permettra de bénéfificier de réduction produit, cadeaux... Ne sera pas affiché publiquement</p>
                    <div className="field padding-top-20">
                        <label className="title" htmlFor="profile_date"></label>
                        <input
                            ref='profile_date'
                            name="profile_date"
                            id="profile_date"
                            type="date"
                            defaultValue = {this.state.profile_date}
                        />
                        <div className="has-error">{this.state.profile_dateValMsg}</div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step2;