'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_email: props.getUser().profile_email
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            if (this.props.getUser().profile_email != userInput.profile_email) {
                this.props.updateUser(userInput);
            }

            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }

        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_emailVal: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(data.profile_email), // required: regex w3c uses in html5
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_emailValMsg: val.profile_emailVal ? '' : 'A valid email is required'
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_email: this.refs.profile_email.value
        };
    }
    render() {
        return (
            <div className="inner step step-3">
                <div className="list-fields">
                    <h2>Quel est votre email</h2>
                    <div className="field">
                        <label className="title" htmlFor="profile_email"></label>
                        <input
                            ref="profile_email"
                            name="profile_email"
                            id="profile_email"
                            defaultValue={this.state.profile_email}
                            type="email"
                        />
                        <div className="has-error">{this.state.profile_emailValMsg}</div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step3;