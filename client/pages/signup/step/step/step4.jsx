'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_pass: props.getUser().profile_pass,
            profile_comfirm_pass: props.getUser().profile_comfirm_pass
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            if (this.props.getUser().profile_pass != userInput.profile_pass || this.props.getUser().profile_comfirm_pass != userInput.profile_comfirm_pass) {
                this.props.updateUser(userInput);
            }
            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }
        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_passVal: (data.profile_pass != ''),
            profile_comfirm_passVal: (data.profile_comfirm_pass != ''),
            profile_pass_compareVal: (data.profile_comfirm_pass == data.profile_pass)
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_passValMsg: val.profile_passVal ? '' : 'The password field is required',
            profile_comfirm_passValMsg: val.profile_comfirm_passVal ? '' : 'The confirm password field is required',
            profile_pass_compareValMsg: val.profile_pass_compareVal ? '' : 'The confirm password field is required',
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_pass: this.refs.profile_pass.value,
            profile_comfirm_pass: this.refs.profile_comfirm_pass.value
        };
    }
    render() {
        return (
            <div className="inner step step-4">
                <div className="list-fields">
                    <h2>Créez un mot de passe</h2>
                    <div className="field">
                        <label className="title" htmlFor="profile_pass">mot de passe</label>
                        <input
                            ref="profile_pass"
                            name="profile_pass"
                            id="profile_pass"
                            type="password"
                            defaultValue={this.state.profile_pass}
                        />
                        <div className="has-error">{this.state.profile_passValMsg}</div>
                    </div>
                    <div className="field">
                        <label className="title" htmlFor="profile_comfirm_pass">confirmez mot de passe</label>
                        <input
                            ref="profile_comfirm_pass"
                            name="profile_comfirm_pass"
                            id="profile_comfirm_pass"
                            type="password"
                            defaultValue={this.state.profile_comfirm_pass}
                        />
                        <div className="has-error">{this.state.profile_comfirm_passValMsg}</div>
                        <div className="has-error">{this.state.profile_pass_compareValMsg}</div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step4;