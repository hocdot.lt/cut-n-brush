'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step6 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_phone: props.getUser().profile_phone
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            if (this.props.getUser().profile_phone != userInput.profile_phone) {
                this.props.updateUser(userInput);
            }
            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }
        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_phoneVal: (data.profile_phone != '' && data.profile_phone != '+41')
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_phoneValMsg: val.profile_phoneVal ? '' : 'The password field is required'
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_phone: this.refs.profile_phone.value
        };
    }
    render() {
        return (
            <div className="inner step step-6">
                <div className="list-fields">
                    <h2>Numéro de tél mobile</h2>
                    <p>FORMAT: 79 222 222 22</p>
                    <div className="field">
                        <label className="title" htmlFor="profile_phone"></label>
                        <input
                            ref="profile_phone"
                            name="profile_phone"
                            id="profile_phone"
                            type="tel"
                            defaultValue={this.state.profile_phone}
                        />
                        <div className="has-error">{this.state.profile_phoneValMsg}</div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step6;