'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step8 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_code_porte: props.getUser().profile_code_porte,
            profile_animal_de_compagnie: props.getUser().profile_animal_de_compagnie,
            profile_bleue: props.getUser().profile_bleue,
            profile_blanche: props.getUser().profile_blanche,
            profile_visiteur: props.getUser().profile_visiteur,
            profile_privee: props.getUser().profile_privee,
            profile_no_de_place: props.getUser().profile_no_de_place
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.handleSubmit(event);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            this.props.updateUser(userInput);
            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }
        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            not_check: true
        }
    }

    _validationErrors(val) {
        const errMsgs = {    };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_code_porte: this.refs.profile_code_porte.value,
            profile_animal_de_compagnie: this.refs.profile_animal_de_compagnie.value,
            profile_bleue: this.state.profile_bleue,
            profile_blanche: this.state.profile_blanche,
            profile_visiteur: this.state.profile_visiteur,
            profile_privee: this.state.profile_privee,
            profile_no_de_place: this.refs.profile_no_de_place.value
        };
    }
    updateBleue(e) {
        if(e.currentTarget.checked) {
            this.setState({profile_bleue: e.currentTarget.value});
        }else{
            this.setState({profile_bleue: ''});
        }
    }
    updateBlanche(e) {
        if(e.currentTarget.checked) {
            this.setState({profile_blanche: e.currentTarget.value});
        }else{
            this.setState({profile_blanche: ''});
        }
    }
    updateVisiteur(e) {
        if(e.currentTarget.checked) {
            this.setState({profile_visiteur: e.currentTarget.value});
        }else{
            this.setState({profile_visiteur: ''});
        }
    }
    updatePrivee(e) {
        if(e.currentTarget.checked) {
            this.setState({profile_privee: e.currentTarget.value});
        }else{
            this.setState({profile_privee: ''});
        }
    }
    render() {
        return (
            <div className="inner step step-8">
                <div className="list-fields">
                    <h2>Infos pratiques</h2>
                    <p className="desc">Ces infos ne seront envoyées au coiffeur uniquement
                        qu’après confirmation d’un rendez-vous</p>
                    <div className="field">
                        <label className="title" htmlFor="profile_code_porte">Code porte</label>
                        <input
                            ref="profile_code_porte"
                            name="profile_code_porte"
                            id="profile_code_porte"
                            type="text"
                            defaultValue={this.state.profile_code_porte}
                        />


                    </div>
                    <div className="field">
                        <label className="title" htmlFor="profile_animal_de_compagnie">animal de compagnie ?</label>
                        <input
                            ref="profile_animal_de_compagnie"
                            name="profile_animal_de_compagnie"
                            id="profile_animal_de_compagnie"
                            type="text"
                            defaultValue={this.state.profile_animal_de_compagnie}
                        />

                    </div>
                    <div className="field">
                        <label  className="title">Place de parc ?</label>
                        <div className="field-group">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="checkbox_waper">
                                        <input
                                            ref="profile_bleue"
                                            name="profile_bleue"
                                            id="profile_bleue"
                                            type="checkbox"
                                            checked={this.state.profile_bleue}
                                            defaultValue="1"
                                            onChange={this.updateBleue.bind(this)}
                                        />
                                        <label htmlFor="profile_bleue">Bleue</label>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="checkbox_waper">
                                        <input
                                            ref="profile_blanche"
                                            name="profile_blanche"
                                            id="profile_blanche"
                                            checked={this.state.profile_blanche}
                                            type="checkbox"
                                            defaultValue="1"
                                            onChange={this.updateBlanche.bind(this)}
                                        />
                                        <label htmlFor="profile_blanche">Blanche</label>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="checkbox_waper">
                                        <input
                                            ref="profile_visiteur"
                                            name="profile_visiteur"
                                            id="profile_visiteur"
                                            checked={this.state.profile_visiteur}
                                            type="checkbox"
                                            defaultValue="1"
                                            onChange={this.updateVisiteur.bind(this)}
                                        />
                                        <label htmlFor="profile_visiteur">Visiteur</label>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="checkbox_waper">
                                        <input
                                            ref="profile_privee"
                                            name="profile_privee"
                                            id="profile_privee"
                                            checked={this.state.profile_privee}
                                            type="checkbox"
                                            defaultValue="1"
                                            onChange={this.updatePrivee.bind(this)}
                                        />
                                        <label htmlFor="profile_privee">Privée</label>
                                    </div>
                                </div>
                                <div className="col-sm-8">
                                    <input
                                        ref="profile_no_de_place"
                                        name="profile_no_de_place"
                                        id="profile_no_de_place"
                                        type="text"
                                        placeholder="No de place"
                                        defaultValue={this.state.profile_no_de_place}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="submit">
                    <input type="button" href="#" defaultValue="terminer L’inscription" onClick={this.handleSubmit.bind(this)}/>
                </div>
            </div>
        )
    }
}


module.exports = Step8;