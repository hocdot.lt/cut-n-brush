'use strict';
const React = require('react');
const ReactDOM = require('react-dom');

class Step6 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_np: props.getUser().profile_np,
            profile_localtion: props.getUser().profile_localtion,
            profile_no: props.getUser().profile_no,
            profile_rue: props.getUser().profile_rue
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }
    isValidated() {
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        let isDataValid = false;
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            this.props.updateUser(userInput);
            isDataValid = true;
        }
        else {
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }
        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_npVal: (data.profile_np != ''),
            profile_localtionVal: (data.profile_localtion != ''),
            profile_noVal: (data.profile_no != ''),
            profile_rueVal: (data.profile_rue != '')
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_npValMsg: val.profile_npVal ? '' : 'The NP field is required',
            profile_localtionValMsg: val.profile_localtionVal ? '' : 'The Localité field is required',
            profile_noValMsg: val.profile_noVal ? '' : 'The No field is required',
            profile_rueValMsg: val.profile_rueVal ? '' : 'The Rue field is required'
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_np: this.refs.profile_np.value,
            profile_localtion: this.refs.profile_localtion.value,
            profile_no: this.refs.profile_no.value,
            profile_rue: this.refs.profile_rue.value
        };
    }
    render() {
        return (
            <div className="inner step step-7">
                <div className="list-fields">
                    <h2>Adresse</h2>
                    <div className="fieldGroup">
                        <div className="field first-field">
                            <label className="title" htmlFor="profile_np">NP</label>
                            <input
                                ref="profile_np"
                                name="profile_np"
                                id="profile_np"
                                type="text"
                                defaultValue={this.state.profile_np}
                            />
                        </div>
                        <div className="field">
                            <label className="title" htmlFor="profile_localtion">Localité</label>
                            <input
                                ref="profile_localtion"
                                name="profile_localtion"
                                id="profile_localtion"
                                type="text"
                                defaultValue={this.state.profile_localtion}
                            />

                        </div>
                        <div className="has-error">
                            <span>{this.state.profile_npValMsg}</span>
                            <span>{this.state.profile_localtionValMsg}</span>
                        </div>
                    </div>
                    <div className="fieldGroup">
                        <div className="field first-field">
                            <label className="title" htmlFor="profile_no">No</label>
                            <input
                                ref="profile_no"
                                name="profile_no"
                                id="profile_no"
                                type="text"
                                defaultValue={this.state.profile_no}
                            />

                        </div>
                        <div className="field">
                            <label className="title" htmlFor="profile_rue">Rue</label>
                            <input
                                ref="profile_rue"
                                name="profile_rue"
                                id="profile_rue"
                                type="text"
                                defaultValue={this.state.profile_rue}
                            />
                        </div>
                        <div className="has-error">
                            <span>{this.state.profile_noValMsg}</span>
                            <span>{this.state.profile_rueValMsg}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step6;