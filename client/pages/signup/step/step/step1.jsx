'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
const InputText = require('../../../../components/form/input-text.jsx');
const SelectElement = require('../../../../components/form/select-element.jsx');

class Step1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_gender: props.getUser().profile_gender,
            profile_etudiant: props.getUser().profile_etudiant,
            profile_prenom: props.getUser().profile_prenom,
            profile_nom: props.getUser().profile_nom
        };
        this._validateOnDemand = true;
        this.validationCheck = this.validationCheck.bind(this);
        this.isValidated = this.isValidated.bind(this);
    }

    isValidated() {
        const userInput = this._grabUserInput(); // grab user entered vals
        const validateNewInput = this._validateData(userInput); // run the new input against the validator
        let isDataValid = false;

        // if full validation passes then save to store and pass as valid
        if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === true })) {
            if (this.props.getUser().profile_gender != userInput.profile_gender
                || this.props.getUser().profile_prenom != userInput.profile_prenom
                || this.props.getUser().profile_etudiant != userInput.profile_etudiant
                || this.props.getUser().profile_nom != userInput.profile_nom) {
                this.props.updateUser(userInput);  // Update store here (this is just an example, in reality you will do it via redux or flux)
            }

            isDataValid = true;
        }
        else {
            // if anything fails then update the UI validation state but NOT the UI Data State
            this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
        }

        return isDataValid;
    }

    validationCheck() {
        if (!this._validateOnDemand)
            return;
        const userInput = this._grabUserInput();
        const validateNewInput = this._validateData(userInput);
        this.setState(Object.assign(userInput, validateNewInput, this._validationErrors(validateNewInput)));
    }

    _validateData(data) {
        return  {
            profile_genderVal: (data.profile_gender != ''),
            profile_prenomVal: (data.profile_prenom != ''),
            profile_nomVal: (data.profile_nom != '')
        }
    }

    _validationErrors(val) {
        const errMsgs = {
            profile_genderValMsg: val.profile_genderVal ? '' : 'A gender selection is required',
            profile_prenomValMsg: val.profile_prenomVal ? '' : 'A prénom is required',
            profile_nomValMsg: val.profile_nomVal ? '' : 'A nom is required'
        };
        return errMsgs;
    }

    _grabUserInput() {
        return {
            profile_gender: this.state.profile_gender,
            profile_prenom: this.refs.profile_prenom.value,
            profile_nom: this.refs.profile_nom.value,
            profile_etudiant: this.state.profile_etudiant
        };
    }

    updateGenderval(e){
        this.setState({profile_gender: e.currentTarget.value});
    }

    updateEtudiant(e){
        if(e.currentTarget.checked) {
            this.setState({profile_etudiant: e.currentTarget.value});
        }else{
            this.setState({profile_etudiant: ''});
        }
    }



    render() {
        return (
            <div className="inner step step-1">
                <div className="list-fields">
                    <div className="field">
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="radio_waper">
                                    <InputText
                                        name="profile_gender"
                                        id="profile_femme"
                                        type="radio"
                                        defaultValue="Femme"
                                        ischecked={this.state.profile_gender === 'Femme'}
                                        onChange={this.updateGenderval.bind(this)}
                                    />
                                    <label htmlFor="profile_femme">Femme</label>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="radio_waper">
                                    <InputText
                                        name="profile_gender"
                                        id="profile_homme"
                                        type="radio"
                                        defaultValue="Homme"
                                        ischecked={this.state.profile_gender === 'Homme'}
                                        onChange={this.updateGenderval.bind(this)}
                                    />
                                    <label htmlFor="profile_homme">Homme</label>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="checkbox_waper">
                                    <input
                                        name="profile_etudiant"
                                        id="profile_etudiant"
                                        type="checkbox"
                                        checked={this.state.profile_etudiant}
                                        defaultValue="1"
                                        onChange={this.updateEtudiant.bind(this)}
                                    />
                                    <label htmlFor="profile_etudiant">Etudiant(e)</label>
                                </div>
                            </div>
                        </div>
                        <div className="has-error">{this.state.profile_genderValMsg}</div>
                    </div>
                    <h2>Quel est votre nom ?</h2>
                    <div className="field">
                        <label htmlFor="profile_prenom" className="title ">Prénom</label>
                        <input
                            ref="profile_prenom"
                            name="profile_prenom"
                            id="profile_prenom"
                            type="text"
                            required
                            defaultValue={this.state.profile_prenom}
                            onBlur={this.validationCheck}
                        />
                        <div className="has-error">{this.state.profile_prenomValMsg}</div>
                    </div>
                    <div className="field">
                        <label htmlFor="profile_nom" className="title ">Nom</label>
                        <input
                            ref="profile_nom"
                            name="profile_nom"
                            id="profile_nom"
                            defaultValue={this.state.profile_nom}
                            required
                            type="text"
                            onBlur={this.validationCheck}
                        />
                        <div className="has-error">{this.state.profile_nomValMsg}</div>
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step1;