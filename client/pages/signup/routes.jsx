'use strict';
const NotFound = require('./not-found.jsx');
//const Home = require('./home/index.jsx');
const SingnFacebookPage = require('./facebook/index.jsx');
const MultiStep = require('./step/index.jsx');

const React = require('react');
const ReactRouter = require('react-router');


const Route = ReactRouter.Route;
const Router = ReactRouter.Router;
const browserHistory = ReactRouter.browserHistory;


const Routes = (
    <Router history={browserHistory}>
        <Route path="/signup" component={MultiStep} />
        <Route path="/signup/step" component={MultiStep} />
        <Route path="/signup/facebook" component={SingnFacebookPage} />
        <Route path="*" component={NotFound} />
    </Router>
);


module.exports = Routes;
