'use strict';
const React = require('react');
const ReactRouter = require('react-router');
const $ = require('jquery');
const Actions = require('../actions');
const Store = require('./store');




class SingnFacebookPage extends React.Component {
    componentWillMount() {
        $(document).ready(function(){
           $('#logo').css('display','none');
           $('#page').css('background','none');

        });
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '851691701648286',
                cookie     : true,
                xfbml      : true,
                version    : 'v2.8'
            });
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
                console.log(2);
            });

        };
        function statusChangeCallback(response) {
            console.log(3);
            //this.statusChangeCallback(response);
            if (response.status === 'connected') {
                getDataFbAPI();
            } else {
                var link_fb_login = 'https://www.facebook.com/dialog/oauth/?client_id=851691701648286&redirect_uri=http://localhost:8000/signup/facebook';
                window.location.replace(link_fb_login);
            }
        }
        function getDataFbAPI() {
            FB.api('/me','post',{fields: 'public_key,last_name,first_name,email,gender,picture,cover '}, function(response) {
                Actions.sendRequestFacebook(response);
            });
        }
        this.loadFbSdk(document, 'script', 'facebook-jssdk');



    }

    loadFbSdk(d,s,id){
        console.log(1);
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }

    render() {

        return(
            <div className="facebook-loading"></div>
        );
    }
}
module.exports = SingnFacebookPage;
