/* global window */
'use strict';
const ApiActions = require('../../../actions/api');
const Constants = require('./constants');
const ReactRouter = require('react-router');
const Store = require('./store');


class Actions {
    static sendRequest(data) {

        ApiActions.post(
            '/api/signup',
            data,
            Store,
            Constants.REGISTER,
            Constants.REGISTER_RESPONSE,
            (err, response) => {
                if (!err) {
                    window.location.href = '/';
                }
            }
        );

        ApiActions.post(
            '/api/facebook-login',
            data,
            Store,
            Constants.REGISTER,
            Constants.REGISTER_RESPONSE,
            (err, response) => {
                if (!err) {
                    //window.location.href = '/';
                }else{
                    if(response.message == 'facbook_login'){
                        //window.location.href = '/login';
                    }
                }
            }
        );
    }
};


module.exports = Actions;