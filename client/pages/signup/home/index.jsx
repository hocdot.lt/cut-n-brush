'use strict';
const React = require('react');
const ReactRouter = require('react-router');
const $ = require('jquery');
const Actions = require('../actions');
const Store = require('./store');
const Step1 = require('./step/step1.jsx');
const InputText = require('../../../components/form/input-text.jsx');
const SelectElement = require('../../../components/form/select-element.jsx');


class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.input = {};
        this.state = Store.getState();

    }

    nextStep(step_next,event){
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        $('.signup .step').removeClass('active');
        $(step_next).addClass('active');


    }


    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        console.log(this.input);
        Actions.sendRequest({
            profile_email: this.input.profile_email.value(),
            profile_pass: this.input.profile_pass.value(),
            profile_gender: this.input.profile_gender.value(),
            profile_prenom: this.input.profile_prenom.value(),
            profile_etudiant: this.input.profile_etudiant.value(),
            profile_nom: this.input.profile_nom.value(),
            profile_date: this.input.profile_date.value(),
            profile_cheveux_1: this.input.profile_cheveux_1.value(),
            profile_cheveux_2: this.input.profile_cheveux_2.value(),
            profile_cheveux_3: this.input.profile_cheveux_3.value(),
            profile_phone: this.input.profile_phone.value(),
            profile_np: this.input.profile_np.value(),
            profile_localtion: this.input.profile_localtion.value(),
            profile_no: this.input.profile_no.value(),
            profile_rue: this.input.profile_rue.value(),
            profile_code_porte: this.input.profile_code_porte.value(),
            profile_animal_de_compagnie: this.input.profile_animal_de_compagnie.value(),
            profile_bleue: this.input.profile_bleue.value(),
            profile_blanche: this.input.profile_blanche.value(),
            profile_visiteur: this.input.profile_visiteur.value(),
            profile_privee: this.input.profile_privee.value(),
            profile_no_de_place: this.input.profile_no_de_place.value()
        });
    }
    render() {
        return (
            <form action="" type="POST" className="signup" onSubmit={this.handleSubmit.bind(this)} >
                <Step1/>
                <div className="inner step step-1 active">
                    <div className="list-fields">
                        <div className="field">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="radio_waper">
                                        <InputText
                                            ref={(c) => (this.input.profile_gender = c)}
                                            name="profile_gender"
                                            id="profile_femme"
                                            type="radio"
                                            value="Femme"
                                        />
                                        <label htmlFor="profile_femme">Femme</label>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="radio_waper">
                                        <InputText
                                            ref={(c) => (this.input.profile_gender = c)}
                                            name="profile_gender"
                                            id="profile_homme"
                                            type="radio"
                                            value="Homme"
                                        />
                                        <label htmlFor="profile_homme">Homme</label>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="checkbox_waper">
                                        <InputText
                                            ref={(c) => (this.input.profile_etudiant = c)}
                                            name="profile_etudiant"
                                            id="profile_etudiant"
                                            type="checkbox"
                                            value="yes"
                                        />
                                        <label htmlFor="profile_etudiant">Etudiant(e)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2>Quel est votre nom ?</h2>
                        <div className="field">
                            <label htmlFor="profile_prenom" className="title ">Prénom</label>
                            <InputText
                                ref={(c) => (this.input.profile_prenom = c)}
                                name="profile_prenom"
                                id="profile_prenom"
                                type="text"
                            />
                        </div>
                        <div className="field">
                            <label htmlFor="profile_nom" className="title ">Nom</label>
                            <InputText
                                ref={(c) => (this.input.profile_nom = c)}
                                name="profile_nom"
                                id="profile_nom"
                                type="text"
                            />
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-2" onClick={this.nextStep.bind(this,'.step-2')}></a>
                        </div>
                    </div>
                </div>
                <div className="inner step step-2">
                    <a className="pre-step" href="/signup/step-1" onClick={this.nextStep.bind(this,'.step-1')}></a>
                    <div className="list-fields">
                        <h2>Quel est votre date de naissance ?</h2>
                        <p className="desc">Cette information vous permettra de bénéfificier de réduction produit, cadeaux... Ne sera pas affiché publiquement</p>
                        <div className="field padding-top-20">
                            <label className="title" htmlFor="profile_date"></label>
                            <InputText
                                ref={(c) => (this.input.profile_date = c)}
                                name="profile_date"
                                id="profile_date"
                                type="date"
                            />
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-3" onClick={this.nextStep.bind(this,'.step-3')}></a>
                        </div>
                    </div>
                </div>
                <div className="inner step step-3">
                    <a className="pre-step" href="/signup/step-2" onClick={this.nextStep.bind(this,'.step-2')}></a>
                    <div className="list-fields">
                        <h2>Quel est votre email</h2>
                        <div className="field">
                            <label className="title" htmlFor="profile_email"></label>
                            <InputText
                                ref={(c) => (this.input.profile_email = c)}
                                name="profile_email"
                                id="profile_email"
                                type="email"
                            />
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-4" onClick={this.nextStep.bind(this,'.step-4')}></a>
                        </div>
                    </div>
                </div>
                <div className="inner step step-4">
                    <a className="pre-step" href="/signup/step-3" onClick={this.nextStep.bind(this,'.step-3')}></a>
                    <div className="list-fields">
                        <h2>Créez un mot de passe</h2>
                        <div className="field">
                            <label className="title" htmlFor="profile_pass">mot de passe</label>
                            <InputText
                                ref={(c) => (this.input.profile_pass = c)}
                                name="profile_pass"
                                id="profile_pass"
                                type="password"
                            />
                        </div>
                        <div className="field">
                            <label className="title" htmlFor="profile_comfirm_pass">confirmez mot de passe</label>
                            <InputText
                                ref={(c) => (this.input.profile_comfirm_pass = c)}
                                name="profile_comfirm_pass"
                                id="profile_comfirm_pass"
                                type="password"
                            />
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-5" onClick={this.nextStep.bind(this,'.step-5')}></a>
                        </div>
                    </div>
                </div>
                <div className="inner step step-5">
                    <a className="pre-step" href="/signup/step-4" onClick={this.nextStep.bind(this,'.step-4')}></a>
                    <div className="list-fields">
                        <h2>Merci de compléter votre profil</h2>
                        <div className="bron_irene">
                            <div className="desc">
                                <h2>Bron Irène</h2>
                                <p>Ajouter une photo de profil</p>
                            </div>
                            <div className="upload_image">
                                <span>PHOTO</span>
                            </div>
                        </div>
                        <h2>Type de cheveux</h2>
                        <div className="fieldGroup">
                            <div className="field">
                                <SelectElement
                                    ref={(c) => (this.input.profile_cheveux_1 = c)}
                                    name="profile_cheveux_1"
                                >
                                    <option value="court">Court</option>
                                    <option value="mi-long">mi-long</option>
                                    <option value="long">long</option>
                                </SelectElement>
                            </div>
                            <div className="field">
                                <SelectElement
                                    name="profile_cheveux_2"
                                    ref={(c) => (this.input.profile_cheveux_2 = c)}
                                >
                                    <option value="raide">Raide</option>
                                    <option value="raide-2">Raide-2</option>
                                    <option value="raide-3">Raide-3</option>
                                </SelectElement>
                            </div>
                            <div className="field">
                                <SelectElement
                                    name="profile_cheveux_3"
                                    ref={(c) => (this.input.profile_cheveux_3 = c)}
                                >
                                    <option value="fin">Fin</option>
                                    <option value="fin-1">Fin-1</option>
                                    <option value="fin-2">Fin-2</option>
                                </SelectElement>
                            </div>
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-6" onClick={this.nextStep.bind(this,'.step-6')}></a>
                            <h3>PLUS QUE 3 ÉTAPE</h3>
                            <div className="processing">
                                <div><span className="active"></span></div>
                                <div><span></span></div>
                                <div><span></span></div>
                                <div><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="inner step step-6">
                    <a className="pre-step" href="/signup/step-5" onClick={this.nextStep.bind(this,'.step-5')}></a>
                    <div className="list-fields">
                        <h2>Numéro de tél mobile</h2>
                        <p>FORMAT: 79 222 222 22</p>
                        <div className="field">
                            <label className="title" htmlFor="profile_phone"></label>
                            <InputText
                                ref={(c) => (this.input.profile_phone = c)}
                                name="profile_phone"
                                id="profile_phone"
                                type="tel"
                                value=""
                            />
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-7" onClick={this.nextStep.bind(this,'.step-7')}></a>
                            <h3>PLUS QUE 2 ÉTAPE</h3>
                            <div className="processing">
                                <div><span className="active"></span></div>
                                <div><span className="active"></span></div>
                                <div><span></span></div>
                                <div><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="inner step step-7">
                    <a className="pre-step" href="/signup/step-6" onClick={this.nextStep.bind(this,'.step-6')}></a>
                    <div className="list-fields">
                        <h2>Adresse</h2>
                        <div className="fieldGroup">
                            <div className="field first-field">
                                <label className="title" htmlFor="profile_np">NP</label>
                                <InputText
                                    ref={(c) => (this.input.profile_np = c)}
                                    name="profile_np"
                                    id="profile_np"
                                    type="text"
                                />
                            </div>
                            <div className="field">
                                <label className="title" htmlFor="profile_localtion">Localité</label>
                                <InputText
                                    ref={(c) => (this.input.profile_localtion = c)}
                                    name="profile_localtion"
                                    id="profile_localtion"
                                    type="text"
                                />

                            </div>
                        </div>
                        <div className="fieldGroup">
                            <div className="field first-field">
                                <label className="title" htmlFor="profile_no">No</label>
                                <InputText
                                    ref={(c) => (this.input.profile_no = c)}
                                    name="profile_no"
                                    id="profile_no"
                                    type="text"
                                />

                            </div>
                            <div className="field">
                                <label className="title" htmlFor="profile_rue">Rue</label>
                                <InputText
                                    ref={(c) => (this.input.profile_rue = c)}
                                    name="profile_rue"
                                    id="profile_rue"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="field next-step">
                            <a href="/signup/step-8" onClick={this.nextStep.bind(this,'.step-8')}></a>
                            <h3>PLUS QUE 1 ÉTAPE</h3>
                            <div className="processing">
                                <div><span className="active"></span></div>
                                <div><span className="active"></span></div>
                                <div><span className="active"></span></div>
                                <div><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="inner step step-8">
                    <a className="pre-step" href="/signup/step-7" onClick={this.nextStep.bind(this,'.step-7')}></a>
                    <div className="list-fields">
                        <h2>Infos pratiques</h2>
                        <p className="desc">Ces infos ne seront envoyées au coiffeur uniquement
                            qu’après confirmation d’un rendez-vous</p>
                        <div className="field">
                            <label className="title" htmlFor="profile_code_porte">Code porte</label>
                            <InputText
                                ref={(c) => (this.input.profile_code_porte = c)}
                                name="profile_code_porte"
                                id="profile_code_porte"
                                type="text"
                            />


                        </div>
                        <div className="field">
                            <label className="title" htmlFor="profile_animal_de_compagnie">animal de compagnie ?</label>
                            <InputText
                                ref={(c) => (this.input.profile_animal_de_compagnie = c)}
                                name="profile_animal_de_compagnie"
                                id="profile_animal_de_compagnie"
                                type="text"
                            />

                        </div>
                        <div className="field">
                            <label  className="title">Place de parc ?</label>
                            <div className="field-group">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div className="checkbox_waper">
                                            <InputText
                                                ref={(c) => (this.input.profile_bleue = c)}
                                                name="profile_bleue"
                                                id="profile_bleue"
                                                type="checkbox"
                                            />
                                            <label htmlFor="profile_bleue">Bleue</label>
                                        </div>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="checkbox_waper">
                                            <InputText
                                                ref={(c) => (this.input.profile_blanche = c)}
                                                name="profile_blanche"
                                                id="profile_blanche"
                                                type="checkbox"
                                            />
                                            <label htmlFor="profile_blanche">Blanche</label>
                                        </div>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="checkbox_waper">
                                            <InputText
                                                ref={(c) => (this.input.profile_visiteur = c)}
                                                name="profile_visiteur"
                                                id="profile_visiteur"
                                                value="Visiteur"
                                                type="checkbox"
                                            />
                                            <label htmlFor="profile_visiteur">Visiteur</label>
                                        </div>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="checkbox_waper">
                                            <InputText
                                                ref={(c) => (this.input.profile_privee = c)}
                                                name="profile_privee"
                                                id="profile_privee"
                                                value="Privée"
                                                type="checkbox"
                                            />
                                            <label htmlFor="profile_privee">Privée</label>
                                        </div>
                                    </div>
                                    <div className="col-sm-8">
                                        <InputText
                                            ref={(c) => (this.input.profile_no_de_place = c)}
                                            name="profile_no_de_place"
                                            id="profile_no_de_place"
                                            type="text"
                                            placeholder="No de place"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="submit">
                        <input type="submit" value="terminer L’inscription"/>
                    </div>
                </div>
            </form>
        );
    }
}
module.exports = HomePage;
