'use strict';
const React = require('react');
const ReactDOM = require('react-dom');
const InputText = require('../../../../components/form/input-text.jsx');
const SelectElement = require('../../../../components/form/select-element.jsx');

class Step1 extends React.Component {

    render() {
        return (
            <div className="inner step step-1 active">
                <div className="list-fields">
                    <div className="field">
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="radio_waper">
                                    <InputText
                                        ref={(c) => (this.input.profile_gender = c)}
                                        name="profile_gender"
                                        id="profile_femme"
                                        type="radio"
                                        value="Femme"
                                    />
                                    <label htmlFor="profile_femme">Femme</label>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="radio_waper">
                                    <InputText
                                        ref={(c) => (this.input.profile_gender = c)}
                                        name="profile_gender"
                                        id="profile_homme"
                                        type="radio"
                                        value="Homme"
                                    />
                                    <label htmlFor="profile_homme">Homme</label>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="checkbox_waper">
                                    <InputText
                                        ref={(c) => (this.input.profile_etudiant = c)}
                                        name="profile_etudiant"
                                        id="profile_etudiant"
                                        type="checkbox"
                                        value="yes"
                                    />
                                    <label htmlFor="profile_etudiant">Etudiant(e)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2>Quel est votre nom ?</h2>
                    <div className="field">
                        <label htmlFor="profile_prenom" className="title ">Prénom</label>
                        <InputText
                            ref={(c) => (this.input.profile_prenom = c)}
                            name="profile_prenom"
                            id="profile_prenom"
                            type="text"
                        />
                    </div>
                    <div className="field">
                        <label htmlFor="profile_nom" className="title ">Nom</label>
                        <InputText
                            ref={(c) => (this.input.profile_nom = c)}
                            name="profile_nom"
                            id="profile_nom"
                            type="text"
                        />
                    </div>
                </div>
            </div>
        )
    }
}


module.exports = Step1;