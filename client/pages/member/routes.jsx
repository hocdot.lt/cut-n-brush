'use strict';
const NotFound = require('./not-found.jsx');
const HairdresserPage = require('./hairdresser/index.jsx');
const UserPage = require('./client/index.jsx');

const React = require('react');
const ReactRouter = require('react-router');


const Route = ReactRouter.Route;
const Router = ReactRouter.Router;
const browserHistory = ReactRouter.browserHistory;


const Routes = (
    <Router history={browserHistory}>
        <Route path="/member" component={HairdresserPage} />
        <Route path="/member/hairdresser" component={HairdresserPage} />
        <Route path="/member/client" component={UserPage} />
        <Route path="*" component={NotFound} />
    </Router>
);


module.exports = Routes;
