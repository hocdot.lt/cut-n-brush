'use strict';

const ReactRouter = require('react-router');
const React = require('react');
const Actions = require('../actions');
const Store = require('../store');
const Async = require('async');
import BurgerMenu from 'react-burger-menu';
const Moment = require('moment');

class UserPage extends React.Component {

    constructor(props) {
        super(props);

        Actions.getDetails();
        Actions.getUser();
        this.state = Store.getState();

    }
    componentDidMount() {

        this.unsubscribeStore = Store.subscribe(this.onStoreChange.bind(this));
    }

    componentWillUnmount() {

        this.unsubscribeStore();
    }

    onStoreChange() {

        this.setState(Store.getState());
    }
    getBrithday(){
        const thisMoment = Moment(this.state.profile_date);
        return thisMoment.format('DD') + thisMoment.format('MMM') + thisMoment.format('YYYY')
    }
    render() {
        let _show_date = '';
        if(this.state.profile_date) {
            let _date = Moment(this.state.profile_date);
            _date.locale('fr');
            _show_date = _date.format('DD') + ' ' + _date.format('MMM') + ' ' + _date.format('YYYY');
        }
        const Menu = BurgerMenu['pushRotate'];
        return (
            <div className="member-page inner-app" id="inner-app">
                <Menu  id={"pushRotate"} right outerContainerId={"inner-app"} pageWrapId={'menu-wrap'} width={ '320px' }>
                    <h2>
                        <img src={this.state.profile_avartar}/>
                        <p>{this.state.profile_prenom} {this.state.profile_nom}</p>
                    </h2>
                    <a id="home" className="menu-item" href="/">ACCUEIL</a>
                    <a id="notification" className="menu-item" href="/about">Notification</a>
                    <a id="profile" className="menu-item" href="/about">Profile</a>
                    <a id="images" className="menu-item" href="/about">Images</a>
                    <a id="calendar-setting" className="menu-item" href="/about">Calendar setting</a>
                    <a id="orders" className="menu-item" href="/about">Orders</a>
                    <a id="reviews" className="menu-item" href="/about">Revies</a>
                    <a id="logout" className="menu-item" href="/logout">Logout</a>
                </Menu>
                <div id="menu-wrap">
                    <div className="header-page black">
                        <h3 className="page-title">Profile</h3>
                    </div>
                    <div className="logo"><a href="/"><img src="/public/media/logo-inner.png"/></a></div>
                    <div className="inner-page">
                        <div className="content">
                            <div className="cover-image">
                                <img src={this.state.profile_cover_image}/>
                            </div>
                            <div className="member-name">
                                <h1 className="name">{this.state.profile_prenom} {this.state.profile_nom}</h1>
                                <p className="service">{_show_date}</p>
                            </div>
                            <div className="member-infomation">
                                <div className="info info-1">
                                    <div className="header-info">
                                        <h3 className="title">Type de cheveux</h3>
                                        <a className="edit-info" href="#"></a>
                                    </div>
                                    <div className="info-content">
                                        <div className="field">
                                            <span className="label">Longeur</span>
                                            <span className="value">{this.state.profile_cheveux.profile_cheveux_1}</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Structure</span>
                                            <span className="value">{this.state.profile_cheveux.profile_cheveux_2}</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Epaisseur</span>
                                            <span className="value">{this.state.profile_cheveux.profile_cheveux_3}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="info info-2">
                                    <div className="header-info">
                                        <h3 className="title">Adresse</h3>
                                        <a className="edit-info" href="#"></a>
                                    </div>
                                    <div className="info-content">
                                        <div className="field">
                                            <span className="label">Rue</span>
                                            <span className="value">{this.state.profile_rue}</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Code postal</span>
                                            <span className="value">{this.state.profile_np}</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Ville</span>
                                            <span className="value">Belmont</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Pays</span>
                                            <span className="value">Suisse</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Type d’habitation</span>
                                            <span className="value">Maison</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Code porte</span>
                                            <span className="value">{this.state.profile_code_porte}</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Etage</span>
                                            <span className="value">-</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Place de parc</span>
                                            <span className="value">privée</span>
                                        </div>
                                        <div className="field">
                                            <span className="label">Animaux</span>
                                            <span className="value">Chat, chien</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className=""></div>
                    </div>
                    <div className="footer-menu">
                        <ul className="footer-nav">
                            <li><a href="#"><p><img src="/public/media/produits-icon.png" /></p><span>produits</span></a></li>
                            <li><a href="#"><p><img src="/public/media/heart-icon.png" /></p><span>FAVORIS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/scissor-icon.png"/></p><span>COIFFEURS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/clock-icon.png"/></p><span>RENDEZ-VOUS</span></a></li>
                            <li className="profile"><a href="#"><p><img src="/public/media/avatar.jpg"/></p><span>PROFIL</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
module.exports = UserPage;
