'use strict';
const ReactDOM = require('react-dom');
const Routes = require('./routes.jsx');


class MPage {
    static blastoff() {

        this.mainElement = ReactDOM.render(
            Routes,
            window.document.getElementById('app-mount')
        );
    }
}

module.exports = MPage;

/* $lab:coverage:off$ */
if (!module.parent) {
    window.page = MPage;
    MPage.blastoff();
}
/* $lab:coverage:on$ */
