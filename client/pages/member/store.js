'use strict';
const Redux = require('redux');
const Constants = require('./constants');
const ObjectAssign = require('object-assign');
const ParseValidation = require('../../helpers/parse-validation');
const initialState = {
    hydrated: false,
    loading: false,
    showSaveSuccess: false,
    error: undefined,
    hasError: {},
    help: {},
    profile_avartar: '',
    profile_cover_image: '',
    profile_gender: '',
    profile_nom: '',
    profile_prenom: '',
    profile_rue: '',
    profile_np: '',
    profile_date: '',
    profile_code_porte: '',
    profile_animal_de_compagnie: '',
    profile_cheveux: {
        profile_cheveux_1:'',
        profile_cheveux_2:'',
        profile_cheveux_3:''
    },
    user: '',
    userdetail: ''
};
const reducer = function (state = initialState, action) {
    console.log(action.type === Constants.GET_USER_RESPONSE);
    if (action.type === Constants.GET_USER) {
        return ObjectAssign({}, state, {
            loading: true,
            hydrated: false
        });
    }
    if (action.type === Constants.GET_DETAILS) {
        return ObjectAssign({}, state, {
            loading: true,
            hydrated: false
        });
    }
    if (action.type === Constants.GET_DETAILS_RESPONSE) {
        const validation = ParseValidation(action.response);
        return ObjectAssign({}, state, {
            loading: false,
            hydrated: true,
            error: validation.error,
            hasError: validation.hasError,
            help: validation.help,
            profile_avartar: action.response.profile_avartar,
            profile_cover_image: action.response.profile_cover_image,
            profile_gender: action.response.profile_gender,
            profile_nom: action.response.profile_nom,
            profile_prenom: action.response.profile_prenom,
            profile_rue: action.response.profile_rue || '-',
            profile_np: action.response.profile_np || '-',
            profile_date: action.response.profile_date || '',
            profile_code_porte: action.response.profile_code_porte || '-',
            profile_animal_de_compagnie: action.response.profile_animal_de_compagnie || '-',
            profile_cheveux: action.response.profile_cheveux || {
                profile_cheveux_1 : '-',
                profile_cheveux_2 : '-',
                profile_cheveux_3 : '-'
            },
            user:action.response.user,
            userdetail:action.response
        });
    }
    return state;
};

module.exports = Redux.createStore(reducer);
