'use strict';

const ReactRouter = require('react-router');
const React = require('react');
import Slider from 'react-slick';
import BurgerMenu from 'react-burger-menu';

class HairdresserPage extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: 1
        };
        const Menu = BurgerMenu['pushRotate'];

        return (
            <div className="member-page inner-app" id="inner-app">
                <Menu  id={"pushRotate"} right outerContainerId={"inner-app"} pageWrapId={'menu-wrap'}>
                    <h2>
                        <img src="/public/media/avatar.jpg"/>
                        <p>Callara Vincent</p>
                    </h2>
                    <a id="home" className="menu-item" href="/">ACCUEIL</a>
                    <a id="notification" className="menu-item" href="/about">Notification</a>
                    <a id="profile" className="menu-item" href="/about">Profile</a>
                    <a id="images" className="menu-item" href="/about">Images</a>
                    <a id="calendar-setting" className="menu-item" href="/about">Calendar setting</a>
                    <a id="orders" className="menu-item" href="/about">Orders</a>
                    <a id="reviews" className="menu-item" href="/about">Revies</a>
                    <a id="logout" className="menu-item" href="/logout">Logout</a>
                </Menu>
                <div id="menu-wrap">
                    <div className="header-page black">
                        <a className="back-page" href="#"></a>
                        <h3 className="page-title">choisir son coiffeur</h3>
                    </div>
                    <div className="logo"><img src="/public/media/logo-inner.png"/></div>
                    <div className="menu-second">
                        <ul>
                            <li><a href="#">Historique coiffeur</a></li>
                            <li><a href="#">Coiffeur du mois</a></li>
                            <li className="search-input">
                                <form action="" method="post">
                                    <input type="text" placeholder="recherche coiffeur" className="search-input"/>
                                    <button type="submit"></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div className="inner-page">
                        <div className="content">
                            <div className="cover-image">
                                <img src="/public/media/cover-image.jpg"/>
                                <div className="services">
                                    <span className="review"><img src="/public/media/white-star.png"/> 5/5</span>
                                    <span className="service-name">prendre rendez-vous</span>
                                    <span className="">120 avis</span>
                                </div>
                            </div>
                            <div className="member-name">
                                <h1 className="name">Callara Vincent</h1>
                                <p className="service">Barbier et Coiffeur hommes / femmes</p>
                            </div>
                            <div className="member-infomation">
                                <Slider {...settings}>
                                    <div className="info info-1 step-info experiences">
                                        <div className="header-info">
                                            <h3 className="title">Expériences</h3>
                                        </div>
                                        <div className="info-content">
                                            <p>3 ans de CFC Coiffure <span>chez</span> Yokoso, Atelier <span>et</span> Salon de La Paix</p>
                                            <p>2 ans BP école de coiffure à <span>Genève</span></p>
                                            <p>à travaillé chez Théatre de la coiffure <span>à lausanne et</span> au Salon Cocoon <span>à Pully</span></p>
                                            <p>Indépendant <span>Cut’N’Brush</span> depuis 2017</p>
                                        </div>
                                    </div>
                                    <div className="info info-2 step-info">
                                        <div className="header-info">
                                            <h3 className="title">Phylosophie de vie</h3>
                                        </div>
                                        <div className="info-content">
                                            <p><b>Notre vie vaut ce qu'elle nous a coute d'efort...</b></p>
                                        </div>
                                    </div>
                                    <div className="info info-3 step-info">
                                        <div className="header-info">
                                            <h3 className="title">Portfolio</h3>
                                        </div>
                                        <div className="info-content">
                                            <p>33 Photos Instagram</p>
                                            <div className="list-image"></div>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                        <div className=""></div>
                    </div>
                    <div className="footer-menu">
                        <ul className="footer-nav">
                            <li className="none-icon"><a href="#"><p></p><span>tendances</span></a></li>
                            <li><a href="#"><p><img src="/public/media/heart-icon.png" /></p><span>FAVORIS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/scissor-icon.png"/></p><span>COIFFEURS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/clock-icon.png"/></p><span>RENDEZ-VOUS</span></a></li>
                            <li className="profile"><a href="#"><p><img src="/public/media/avatar.jpg"/></p><span>PROFIL</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
module.exports = HairdresserPage;
