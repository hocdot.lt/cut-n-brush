'use strict';
const ClassNames = require('classnames');
const ControlGroup = require('./control-group.jsx');
const ObjectAssign = require('object-assign');
const React = require('react');


const propTypes = {
    autoCapitalize: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    hasError: React.PropTypes.bool,
    help: React.PropTypes.string,
    inputClasses: React.PropTypes.object,
    label: React.PropTypes.string,
    name: React.PropTypes.string,
    onChange: React.PropTypes.func,
    onBlur: React.PropTypes.func,
    placeholder: React.PropTypes.string,
    type: React.PropTypes.string,
    value: React.PropTypes.string,
    defaultValue: React.PropTypes.string,
    ischecked: React.PropTypes.bool
};
const defaultProps = {
    type: 'text',
    autoCapitalize: 'off'
};


class InputText extends React.Component {
    focus() {

        return this.input.focus();
    }

    value() {
        return this.input.value;
    }

    render() {

        const inputClasses = ClassNames(ObjectAssign({
            'form-control': true
        }, this.props.inputClasses));
        return (
            <input
                ref={(c) => (this.input = c)}
                type={this.props.type}
                autoCapitalize={this.props.autoCapitalize}
                className={this.props.className}
                name={this.props.name}
                id={this.props.id}
                checked={this.props.ischecked}
                placeholder={this.props.placeholder}
                defaultValue={this.props.defaultValue}
                disabled={this.props.disabled ? 'disabled' : undefined}
                onChange={this.props.onChange}
                onBlur={this.props.onBlur}
            />
        );
    }
}

InputText.propTypes = propTypes;
InputText.defaultProps = defaultProps;


module.exports = InputText;
