'use strict';
const ClassNames = require('classnames');
const ControlGroup = require('./control-group.jsx');
const ObjectAssign = require('object-assign');
const React = require('react');


const propTypes = {
    children: React.PropTypes.node,
    defaultValue: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    hasError: React.PropTypes.bool,
    help: React.PropTypes.string,
    inputClasses: React.PropTypes.object,
    label: React.PropTypes.string,
    multiple: React.PropTypes.string,
    name: React.PropTypes.string,
    onChange: React.PropTypes.func,
    size: React.PropTypes.string,
    value: React.PropTypes.oneOfType([
        React.PropTypes.bool,
        React.PropTypes.string
    ])
};
const defaultProps = {
    type: 'text'
};


class SelectElement extends React.Component {
    value() {

        return this.input.value;
    }

    render() {
        return (

                <select
                    ref={(c) => (this.input = c)}
                    multiple={this.props.multiple}
                    className={this.props.className}
                    name={this.props.name}
                    id={this.props.id}
                    size={this.props.size}
                    value={this.props.value}
                    defaultValue={this.props.defaultValue}
                    disabled={this.props.disabled}
                    onChange={this.props.onChange}>
                    {this.props.children}
                </select>
        );
    }
}

SelectElement.propTypes = propTypes;
SelectElement.defaultProps = defaultProps;


module.exports = SelectElement;
