'use strict';
const Path = require('path');
const Gulp = require('gulp');
const Newer = require('gulp-newer');
const Concat = require('gulp-concat');
const Less = require('gulp-less');


Gulp.task('less', () => {

    const bundleConfigs = [{
        entries: [
            './client/core/bootstrap.less',
            './client/core/font-awesome.less'
        ],
        dest: './public',
        outputName: 'core.min.css'
    }, {
        entries: './client/layouts/default.less',
        dest: './public/layouts',
        outputName: 'default.min.css'
    }, {
        entries: './client/layouts/app.less',
        dest: './public/layouts',
        outputName: 'app.min.css'
    }, {
        entries: './client/pages/account/index.less',
        dest: './public/pages',
        outputName: 'account.min.css'
    }, {
        entries: './client/pages/admin/index.less',
        dest: './public/pages',
        outputName: 'admin.min.css'
    }, {
        entries: './client/pages/home/index.less',
        dest: './public/pages',
        outputName: 'home.min.css'
    },{
        entries: './client/pages/signup/index.less',
        dest: './public/pages',
        outputName: 'signup.min.css'
    },{
        entries: './client/pages/login/index.less',
        dest: './public/pages',
        outputName: 'login.min.css'
    },{
        entries: './client/pages/search-result/index.less',
        dest: './public/pages',
        outputName: 'search.result.min.css'
    },{
        entries: './client/pages/member/index.less',
        dest: './public/pages',
        outputName: 'member.min.css'
    },{
        entries: './client/pages/appointment/index.less',
        dest: './public/pages',
        outputName: 'appointment.min.css'
    },{
        entries: './client/pages/calendar/index.less',
        dest: './public/pages',
        outputName: 'calendar.min.css'
    },{
        entries: './client/pages/register/index.less',
        dest: './public/pages',
        outputName: 'register.min.css'
    }];

    return bundleConfigs.map((bundleConfig) => {

        return Gulp.src(bundleConfig.entries)
            .pipe(Newer(Path.join(bundleConfig.dest, bundleConfig.outputName)))
            .pipe(Concat(bundleConfig.outputName))
            .pipe(Less({ compress: true }))
            .pipe(Gulp.dest(bundleConfig.dest));
    });
});
