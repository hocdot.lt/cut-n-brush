'use strict';
const Gulp = require('gulp');
const Path = require('path');
const Merge = require('merge-stream');


Gulp.task('media', () => {

    const general = Gulp.src('./client/media/**/*')
        .pipe(Gulp.dest(Path.join('./public', 'media')));

    const fonts = Gulp.src('./node_modules/font-awesome/fonts/**')
        .pipe(Gulp.dest(Path.join('./public', 'media', 'font-awesome', 'fonts')));

    const material = Gulp.src('./node_modules/material-components-web/dist/**')
        .pipe(Gulp.dest(Path.join('./public', 'layouts', 'material')));

    return Merge(general, fonts);
});
