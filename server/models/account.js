'use strict';
const Joi = require('joi');
const MongoModels = require('mongo-models');
const NoteEntry = require('./note-entry');
const StatusEntry = require('./status-entry');


class Account extends MongoModels {
    static create(data_account, callback) {
        const document = {
            profile_prenom      :   data_account.profile_prenom,
            profile_nom         :   data_account.profile_nom,
            profile_gender      :   data_account.profile_gender,
            profile_etudiant    :   data_account.profile_etudiant,
            profile_date        :   data_account.profile_date,
            profile_cheveux     : {
                profile_cheveux_1:  data_account.profile_cheveux_1,
                profile_cheveux_2:  data_account.profile_cheveux_2,
                profile_cheveux_3:  data_account.profile_cheveux_3
            },
            profile_phone       :   data_account.profile_phone,
            profile_np          :   data_account.profile_np,
            profile_localtion   :   data_account.profile_localtion,
            profile_no          :   data_account.profile_no,
            profile_rue         :   data_account.profile_rue,


            profile_code_porte          :   data_account.profile_code_porte,
            profile_animal_de_compagnie :   data_account.profile_animal_de_compagnie,
            profile_no_de_place         :   data_account.profile_no_de_place,
            profile_place_de_parc       :   {
                profile_bleue           :   data_account.profile_bleue,
                profile_blanche         :   data_account.profile_blanche,
                profile_visiteur        :   data_account.profile_visiteur,
                profile_privee          :   data_account.profile_privee
            },
            profile_avartar :   data_account.profile_avartar,
            profile_cover_image :   data_account.profile_cover_image,
            timeCreated         :   new Date()
        };

        this.insertOne(document, (err, docs) => {

            if (err) {
                return callback(err);
            }

            callback(null, docs[0]);
        });
    }

    static createbyFaceBook(data_account, callback) {
        const document = {
            profile_prenom      :   data_account.profile_prenom,
            profile_nom         :   data_account.profile_nom,
            profile_gender      :   data_account.profile_gender,
            profile_avartar     :   data_account.profile_avartar,
            profile_cover_image :   data_account.profile_cover_image,
            timeCreated         :   new Date()
        };

        this.insertOne(document, (err, docs) => {

            if (err) {
                return callback(err);
            }

            callback(null, docs[0]);
        });
    }

    static findByUsername(username, callback) {

        const query = { 'user.name': username.toLowerCase() };

        this.findOne(query, callback);
    }
}


Account.collection = 'accounts';


Account.schema = Joi.object().keys({
    _id: Joi.object(),
    user: Joi.object().keys({
        id: Joi.string().required(),
        name: Joi.string().lowercase().required()
    }),

    profile_prenom: Joi.string(),
    profile_nom: Joi.string(),
    profile_gender: Joi.string(),
    profile_etudiant: Joi.string(),
    profile_date: Joi.string(),
    profile_cheveux: Joi.object().keys({
        profile_cheveux_1: Joi.string(),
        profile_cheveux_2: Joi.string(),
        profile_cheveux_3: Joi.string()
    }),
    profile_phone: Joi.string(),
    profile_np: Joi.string(),
    profile_localtion: Joi.string(),
    profile_no: Joi.string(),
    profile_rue: Joi.string(),
    profile_code_porte: Joi.string(),
    profile_animal_de_compagnie: Joi.string(),
    profile_place_de_parc : Joi.object().keys({
        profile_bleue: Joi.string(),
        profile_blanche: Joi.string(),
        profile_visiteur: Joi.string(),
        profile_privee: Joi.string()
    }),
    profile_no_de_place: Joi.string(),
    status: Joi.object().keys({
        current: StatusEntry.schema,
        log: Joi.array().items(StatusEntry.schema)
    }),
    notes: Joi.array().items(NoteEntry.schema),
    verification: Joi.object().keys({
        complete: Joi.boolean(),
        token: Joi.string()
    }),
    profile_avartar :   Joi.string(),
    profile_cover_image :   Joi.string(),
    timeCreated: Joi.date()
});


Account.indexes = [
    { key: { 'user.id': 1 } },
    { key: { 'user.name': 1 } }
];


module.exports = Account;
