'use strict';


exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/public/{param*}',
        handler: {
            directory: {
                path: 'public',
                listing: true
            }
        }
    });
    server.route({
        method: 'GET',
        path: '/node_modules/{param*}',
        handler: {
            directory: {
                path: 'node_modules',
                listing: true
            }
        }
    });


    next();
};


exports.register.attributes = {
    name: 'public'
};
