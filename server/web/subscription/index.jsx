'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');


class SubscriptPage extends React.Component {
    render() {
        return (
            <Layout
                title="Demande d’adhésion"
                activeTab="home">
                <div className="subscript-page inner-app">
                    <div className="header-page black">
                        <a className="back-page" href=""></a>
                        <h3 className="page-title">demande d’adhésion</h3>
                    </div>
                    <div className="inner-page">
                        <div className="logo"><img src="/public/media/logo-inner.png"/></div>
                        <div className="content">
                            <p>Cet espace est dédié uniquement aux professionels de la coiffure désirant devenir indépendant et donc de bénéficier des avantages du label et de l’entreprise CNB.</p>
                            <p>La demande d’adhésion permet à vous et à nous d’établir un profil détaillé sur qui vous êtes et ainsi fixé un rendez-vous. </p>
                            <p>Intéressé ?<br/>Remplissez les 6 petites étapes... </p>
                        </div>
                        <div className="action">
                            <button className="submit-btn btn-full btn-gray">Suivant</button>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}


module.exports = SubscriptPage;
