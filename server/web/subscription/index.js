'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/subscription',
        handler: function (request, reply) {
            return reply.view('subscription/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/subscription'
};
