'use strict';
const Layout = require('../layouts/default.jsx');
const React = require('react');


class LoginPage extends React.Component {
    render() {

        const feet = <script src="/public/pages/login.min.js"></script>;
        const neck = <link rel='stylesheet' href="/public/pages/login.min.css" />;

        return (
            <Layout
                title="se connecter"
                feet={feet}
                neck={neck}
                activeTab="login">
                <div id="login-page">
                    <div className="inner">
                        <div id="app-mount"></div>
                    </div>
                </div>
            </Layout>
        );
    }
}


module.exports = LoginPage;
