'use strict';
const Navbar = require('./navbar.jsx');
const React = require('react');
const propTypes = {
    activeTab: React.PropTypes.string,
    children: React.PropTypes.node,
    feet: React.PropTypes.node,
    neck: React.PropTypes.node,
    title: React.PropTypes.string
};
class InnerAppLayout extends React.Component {
    componentWillMount() {

    }
    render() {
        return (
            <html>
            <head>
                <title>{this.props.title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=750,maximum-scale=1, user-scalable=no"/>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,700|Oswald:300,400,500,700|Lato:300,400,500,700" rel="stylesheet"/>
                <link rel="stylesheet" href="/public/layouts/material/material-components-web.css"/>
                <link rel="stylesheet" href="/public/core.min.css" />
                <link rel="stylesheet" href="/public/layouts/app.min.css" />
                <link rel="shortcut icon" href="/public/media/favicon.ico" />
                {this.props.neck}
            </head>
            <body id="body_element">
                <div className="page_inner_app" id="page">
                    {this.props.children}
                </div>
                <script src="/public/core.min.js"></script>
                {this.props.feet}
            </body>
            </html>
        );
    }
}

InnerAppLayout.propTypes = propTypes;


module.exports = InnerAppLayout;
