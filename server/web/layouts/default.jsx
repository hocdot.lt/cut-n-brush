'use strict';
const Navbar = require('./navbar.jsx');
const React = require('react');
//require('material-components-web');

const propTypes = {
    activeTab: React.PropTypes.string,
    children: React.PropTypes.node,
    feet: React.PropTypes.node,
    neck: React.PropTypes.node,
    title: React.PropTypes.string
};
class DefaultLayout extends React.Component {
    componentWillMount() {

    }
    render() {
        return (
            <html>
                <head>
                    <title>{this.props.title}</title>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,700|Oswald:300,400,500,700" rel="stylesheet"/>
                    <link rel="stylesheet" href="/public/layouts/material/material-components-web.css"/>
                    <link rel="stylesheet" href="/public/core.min.css" />
                    <link rel="stylesheet" href="/public/layouts/default.min.css" />
                    <link rel="shortcut icon" href="/public/media/favicon.ico" />
                    {this.props.neck}
                </head>
                <body id="body_element">
                    <div className="page" id="page">
                        <div id="logo">
                            <a href="/">
                                <img
                                    className="navbar-logo"
                                    src="/public/media/logo.png"
                                />
                            </a>
                        </div>
                        <div className="inner-page">{this.props.children}</div>
                    </div>
                    <script src="/public/layouts/material/material-components-web.js"></script>
                    <script>mdc.autoInit()</script>
                    <script src="/public/core.min.js"></script>
                    {this.props.feet}
                </body>
            </html>
        );
    }
}

DefaultLayout.propTypes = propTypes;


module.exports = DefaultLayout;
