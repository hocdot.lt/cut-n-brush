'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');


class CalendarPage extends React.Component {
    render() {

        const neck = <link rel='stylesheet' href="/public/pages/calendar.min.css" />;
        const feet = <script src="/public/pages/calendar.min.js"></script>;
        return (
            <Layout
                title="Prise de rendez-vous"
                neck={neck}
                feet={feet}
                activeTab="home">
                <div id="app-mount"></div>
            </Layout>
        );
    }
}


module.exports = CalendarPage;
