'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/calendar',
        handler: function (request, reply) {
            return reply.view('calendar/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/calendar'
};
