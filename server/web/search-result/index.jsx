'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');

class SearchResultPage extends React.Component {
    render() {
        const neck = <link rel='stylesheet' href="/public/pages/search.result.min.css" />;
        const feet = <script src="/public/pages/search_result.min.js"></script>;
        return (
            <Layout
                title="Choisir son coiffeur"
                neck={neck}
                feet={feet}
                activeTab="home">
                <div id="app-mount"></div>
            </Layout>
        );
    }
}


module.exports = SearchResultPage;
