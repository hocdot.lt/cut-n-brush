'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/search-result',
        handler: function (request, reply) {
            reply.view('search-result/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/search-result'
};