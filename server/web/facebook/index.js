'use strict';
const Bell = require('bell');
exports.register = function (server, options, next) {

    server.auth.strategy('facebook', 'bell', {
        provider: 'facebook',
        password: 'cookie_encryption_password_secure',
        clientId: '851691701648286',
        clientSecret: 'e81fce13bcd752b7a3d588f54300b9ff'
    });


    server.route({
        method: 'GET',
        path: '/facebook',
        config: {
            auth: {
                strategy: 'facebook',
                mode: 'try'
            },
            handler: function (request, reply) {

                if (!request.auth.isAuthenticated) {
                    return reply('Authentication failed due to: ' + request.auth.error.message);
                }
                reply('<pre>' + JSON.stringify(request.auth.credentials, null, 4) + '</pre>');
            }
        }
    });

    next();
};

