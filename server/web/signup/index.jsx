'use strict';
const Layout = require('../layouts/default.jsx');
const React = require('react');


class SignupPage extends React.Component {
    render() {
        const feet = <script src="/public/pages/signup.min.js"></script>;
        const neck = <link rel='stylesheet' href="/public/pages/signup.min.css" />;

        return (
            <Layout
                title="CRÉER UN COMPTE"
                feet={feet}
                neck={neck}
                activeTab="signup">
                <div id="register-page">
                    <div id="app-mount"></div>
                </div>
            </Layout>
        );
    }
}


module.exports = SignupPage;
