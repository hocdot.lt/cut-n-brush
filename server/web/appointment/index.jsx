'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');
import BurgerMenu from 'react-burger-menu';

class AppointmentPage extends React.Component {
    render() {
        const neck = <link rel='stylesheet' href="/public/pages/appointment.min.css" />;
        const feet = <script src="/public/pages/appointment.min.js"></script>;
        const Menu = BurgerMenu['pushRotate'];
        return (
            <Layout
                title="Choisir son coiffeur"
                neck={neck}
                feet={feet}
                activeTab="appointment">
                <div id="app-mount"></div>
            </Layout>
        );
    }
}


module.exports = AppointmentPage;
