'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/appointment',
        handler: function (request, reply) {
            reply.view('appointment/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/appointment'
};
