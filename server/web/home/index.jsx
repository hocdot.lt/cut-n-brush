'use strict';
const Layout = require('../layouts/default.jsx');
const React = require('react');


class HomePage extends React.Component {
    render() {
        const neck = <link rel='stylesheet' href="/public/pages/home.min.css" />;
        return (
            <Layout
                title="CUT&#8217;N&#8217;BRUSH"
                neck={neck}
                activeTab="home">
                <div className="home-page">
                    <div className="inner">
                        <ul className="menu">
                            <li><a href="/signup">CRÉER UN COMPTE</a></li>
                            <li><a href="signup/facebook">Poursuivre avec facebook</a></li>
                            <li><a href="/login">Se connecter</a></li>
                            <li><a href="#">Espace pro</a></li>
                        </ul>
                    </div>
                </div>
            </Layout>
        );
    }
}


module.exports = HomePage;
