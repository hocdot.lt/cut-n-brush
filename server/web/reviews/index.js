'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/reviews',
        handler: function (request, reply) {
            return reply.view('reviews/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/reviews'
};
