'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');


class ReviewsPage extends React.Component {
    render() {
        return (
            <Layout
                title="Choisir son coiffeur"
                activeTab="home">
                <div className="subscript-page inner-app">
                    <div className="header-page black">
                        <a className="back-page" href="#"></a>
                        <h3 className="page-title">choisir son coiffeur</h3>
                        <a className="menu-icon" href="#">
                            <i></i>
                            <i></i>
                            <i></i>
                        </a>
                    </div>
                    <div className="menu-second">
                        <ul>
                            <li><a href="#">Historique coiffeur</a></li>
                            <li><a href="#">Coiffeur du mois</a></li>
                            <li class="search-input">
                                <input type="text" placeholder="recherche coiffeur" class="search-input"/>
                            </li>
                        </ul>
                    </div>
                    <div className="inner-page">
                        <div className="logo"><img src="/public/media/logo-inner.png"/></div>
                        <div className="content">
                            <div className="total-reviews"><h3>6 coiffeurs trouvés selon vos critères</h3></div>
                            <div className="list-reviews">
                                <div className="item">
                                    <div className="avatar">
                                        <img src="/public/media/avatar.jpg"/>
                                    </div>
                                    <div className="info-review">
                                        <h3 className="name">Callara Vincent</h3>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="footer-menu">

                    </div>
                </div>
            </Layout>
        );
    }
}


module.exports = ReviewsPage;
