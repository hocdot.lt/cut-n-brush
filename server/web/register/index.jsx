'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');

class RegisterPage extends React.Component {
    render() {
        const neck = <link rel='stylesheet' href="/public/pages/register.min.css" />;
        const feet = <script src="/public/pages/register.min.js"></script>;
        return (
            <Layout
                title="Choisir son coiffeur"
                neck={neck}
                feet={feet}
                activeTab="register">
                <div id="app-mount"></div>
            </Layout>
        );
    }
}


module.exports = RegisterPage;
