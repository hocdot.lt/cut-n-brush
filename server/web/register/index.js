'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/register',
        handler: function (request, reply) {
            reply.view('register/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/register'
};

