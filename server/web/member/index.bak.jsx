'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');


class MemberPage extends React.Component {
    render() {
        const neck = <link rel='stylesheet' href="/public/pages/member.min.css" />;
        return (
            <Layout
                title="Choisir son coiffeur"
                neck={neck}
                activeTab="home">
                <div className="member-page inner-app">
                    <div className="header-page black">
                        <a className="back-page" href="#"></a>
                        <h3 className="page-title">choisir son coiffeur</h3>
                        <a className="menu-icon" href="#">
                            <i></i>
                            <i></i>
                            <i></i>
                        </a>
                    </div>
                    <div className="logo"><img src="/public/media/logo-inner.png"/></div>
                    <div className="menu-second">
                        <ul>
                            <li><a href="#">Historique coiffeur</a></li>
                            <li><a href="#">Coiffeur du mois</a></li>
                            <li className="search-input">
                                <form action="" method="post">
                                    <input type="text" placeholder="recherche coiffeur" class="search-input"/>
                                    <button type="submit"></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div className="inner-page">
                        <div className="content">
                            <div className="cover-image">
                                <img src="/public/media/cover-image.jpg"/>
                                <div className="services">
                                    <span className="review"><img src="/public/media/white-star.png"/> 5/5</span>
                                    <span className="service-name">prendre rendez-vous</span>
                                    <span className="">120 avis</span>
                                </div>
                            </div>
                            <div className="member-name">
                                <h1 className="name">Callara Vincent</h1>
                                <p class="service">Barbier et Coiffeur hommes / femmes</p>
                            </div>
                            <div className="member-infomation">
                                <div className="info info-1">
                                    <div className="header-info">
                                        <h3 className="title">Expériences</h3>
                                        <a className="next-info" href="#"></a>
                                    </div>
                                    <div className="info-content">
                                        <p>3 ans de CFC Coiffure <span>chez</span> Yokoso, Atelier <span>et</span> Salon de La Paix</p>
                                        <p>2 ans BP école de coiffure à <span>Genève</span></p>
                                        <p>à travaillé chez Théatre de la coiffure <span>à lausanne et</span> au Salon Cocoon <span>à Pully</span></p>
                                        <p>Indépendant <span>Cut’N’Brush</span> depuis 2017</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className=""></div>
                    </div>
                    <div className="footer-menu">
                        <ul className="footer-nav">
                            <li class="none-icon"><a href="#"><p></p><span>tendances</span></a></li>
                            <li><a href="#"><p><img src="/public/media/heart-icon.png" /></p><span>FAVORIS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/scissor-icon.png"/></p><span>COIFFEURS</span></a></li>
                            <li><a href="#"><p><img src="/public/media/clock-icon.png"/></p><span>RENDEZ-VOUS</span></a></li>
                            <li className="profile"><a href="#"><p><img src="/public/media/avatar.jpg"/></p><span>PROFIL</span></a></li>
                        </ul>
                    </div>
                </div>
            </Layout>
        );
    }
}


module.exports = MemberPage;
