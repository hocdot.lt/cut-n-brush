'use strict';
const Layout = require('../layouts/inner-app.jsx');
const React = require('react');


class MemberPage extends React.Component {
    render() {
        const feet = <script src="/public/pages/member.min.js"></script>;
        const neck = <link rel='stylesheet' href="/public/pages/member.min.css" />;

        return (
            <Layout
                title="Member"
                feet={feet}
                neck={neck}
                activeTab="signup">
                    <div id="app-mount"></div>
            </Layout>
        );
    }
}
module.exports = MemberPage;
