'use strict';
exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/member',
        handler: function (request, reply) {
            return reply.view('member/index');
        }
    });
    next();
};


exports.register.attributes = {
    name: 'web/member'
};
