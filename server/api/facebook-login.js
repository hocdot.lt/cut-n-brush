'use strict';
const Async = require('async');
const Boom = require('boom');
const Config = require('../../config');
const Joi = require('joi');
const internals = {};



internals.applyRoutes = function (server, next) {
    const Account = server.plugins['hapi-mongo-models'].Account;
    const Session = server.plugins['hapi-mongo-models'].Session;
    const User = server.plugins['hapi-mongo-models'].User;
    server.route({
        method: 'POST',
        path: '/facebook-login',
        config: {
            plugins: {
                'hapi-auth-cookie': {
                    redirectTo: false
                }
            },
            auth: {
                mode: 'try',
                strategy: 'session'
            },
            validate: {
                payload: {
                    email: Joi.string().allow(''),
                    gender: Joi.string().allow(''),
                    picture: Joi.object().keys({
                        data: Joi.object().keys({
                            is_silhouette:Joi.boolean(),
                            url:Joi.string().allow('')
                        })
                    }),
                    cover: Joi.object().keys({
                        id: Joi.string(),
                        offset_x: Joi.number(),
                        offset_y: Joi.number(),
                        source: Joi.string().allow(''),
                    }),
                    first_name: Joi.string(),
                    last_name: Joi.string(),
                    id: Joi.number()
                }
            },
            pre: [{
                assign: 'emailCheck',
                method: function (request, reply) {
                    const conditions = {
                        email: request.payload.email || request.payload.id.toString()
                    };
                    User.findOne(conditions, (err, user) => {
                        console.log(user);
                        if (err) {
                            return reply(err);
                        }
                        if (user) {


                            return reply(user);
                            //return reply(Boom.conflict('facbook_login'));
                        }
                        reply(true);
                    });
                }
            }]
        },
        handler: function (request, reply) {
            if(!request.pre.emailCheck._id) {
                const mailer = request.server.plugins.mailer;
                Async.auto({
                    user: function (done) {
                        const password = 'welcome_cut_n_brust';
                        const email = request.payload.email || request.payload.id.toString();
                        const username = request.payload.email || request.payload.id.toString();
                        User.create(username, password, email, done);
                    },

                    account: ['user', function (results, done) {
                        var gender = '';
                        if (request.payload.gender == 'male') {
                            gender = 'Homme';
                        } else {
                            gender = 'Femme';
                        }
                        const data_account = {
                            profile_prenom: request.payload.first_name,
                            profile_nom: request.payload.last_name,
                            profile_gender: gender,
                            profile_avartar: request.payload.picture.data.url,
                            profile_cover_image: request.payload.cover.source
                        };

                        Account.createbyFaceBook(data_account, done);
                    }],
                    linkUser: ['account', function (results, done) {
                        const id = results.account._id.toString();
                        const update = {
                            $set: {
                                user: {
                                    id: results.user._id.toString(),
                                    name: results.user.email
                                }
                            }
                        };

                        Account.findByIdAndUpdate(id, update, done);
                    }],
                    linkAccount: ['account', function (results, done) {

                        const id = results.user._id.toString();
                        const update = {
                            $set: {
                                roles: {
                                    account: {
                                        id: results.account._id.toString(),
                                        name: results.account.profile_prenom + ' ' + results.account.profile_nom
                                    }
                                }
                            }
                        };

                        User.findByIdAndUpdate(id, update, done);
                    }],
                    welcome: ['linkUser', 'linkAccount', function (results, done) {

                        const emailOptions = {
                            subject: 'Your ' + Config.get('/projectName') + ' account',
                            to: {
                                name: request.payload.first_name + ' ' + request.payload.last_name,
                                address: request.payload.email
                            }
                        };
                        const template = 'welcome';

                        mailer.sendEmail(emailOptions, template, request.payload, (err) => {

                            if (err) {
                                console.warn('sending welcome email failed:', err.stack);
                            }
                        });

                        done();
                    }],
                    session: ['linkUser', 'linkAccount', function (results, done) {

                        Session.create(results.user._id.toString(), done);
                    }]
                }, (err, results) => {

                    if (err) {
                        return reply(err);
                    }
                    const user = results.linkAccount;
                    const credentials = user.username + ':' + results.session.key;
                    const authHeader = 'Basic ' + new Buffer(credentials).toString('base64');
                    const result = {
                        user: {
                            _id: user._id,
                            username: user.username,
                            email: user.email,
                            roles: user.roles
                        },
                        session: results.session,
                        authHeader
                    };

                    request.cookieAuth.set(result);
                    reply(result);
                });
            }else{
                Async.auto({
                    session:function () {
                        Session.create(request.pre.emailCheck._id.toString(), (err, session) => {
                            if (err) {
                                return reply(err);
                            }
                            const user = request.pre.emailCheck;
                            const credentials = request.pre.emailCheck.username + ':' + session.key;
                            const authHeader = 'Basic ' + new Buffer(credentials).toString('base64');
                            const result = {
                                user: {
                                    _id: user._id,
                                    username: user.username,
                                    email: user.email,
                                    roles: user.roles
                                },
                                session: session,
                                authHeader
                            };

                            request.cookieAuth.set(result);
                            return reply(result);

                        });
                    }
                }, (err, results) => {
                    if (err) {
                        return reply(err);
                    }
                });
            }
        }
    });

    next();
};


exports.register = function (server, options, next) {
    server.dependency(['mailer', 'hapi-mongo-models'], internals.applyRoutes);
    next();
};


exports.register.attributes = {
    name: 'facebook-login'
};
