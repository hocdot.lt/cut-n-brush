'use strict';
const Async = require('async');
const Boom = require('boom');
const Config = require('../../config');
const Joi = require('joi');
const internals = {};


internals.applyRoutes = function (server, next) {

    const Account = server.plugins['hapi-mongo-models'].Account;
    const Session = server.plugins['hapi-mongo-models'].Session;
    const User = server.plugins['hapi-mongo-models'].User;

    server.route({
        method: 'POST',
        path: '/signup',
        config: {
            plugins: {
                'hapi-auth-cookie': {
                    redirectTo: false
                }
            },
            auth: {
                mode: 'try',
                strategy: 'session'
            },
            validate: {
                payload: {
                    profile_email: Joi.string().email().lowercase().required(),
                    profile_pass: Joi.string().required(),
                    profile_gender: Joi.string().required(),
                    profile_prenom: Joi.string(),
                    profile_etudiant: Joi.string().allow(''),
                    profile_nom: Joi.string(),
                    profile_date: Joi.string(),
                    profile_cheveux_1   : Joi.string(),
                    profile_cheveux_2   : Joi.string(),
                    profile_cheveux_3   : Joi.string(),
                    profile_phone       : Joi.string(),
                    profile_np          : Joi.string(),
                    profile_localtion   : Joi.string(),
                    profile_no          : Joi.string(),
                    profile_rue          : Joi.string(),
                    profile_code_porte  : Joi.string().allow(''),
                    profile_animal_de_compagnie : Joi.string().allow(''),
                    profile_bleue       : Joi.string().allow(''),
                    profile_blanche     : Joi.string().allow(''),
                    profile_visiteur    : Joi.string().allow(''),
                    profile_privee      : Joi.string().allow(''),
                    profile_no_de_place : Joi.string().allow(''),
                    profile_avartar : Joi.string().allow(''),
                    profile_cover_image : Joi.string().allow(''),

                }
            },
            pre: [{
                assign: 'emailCheck',
                method: function (request, reply) {
                    const conditions = {
                        email: request.payload.profile_email
                    };
                    User.findOne(conditions, (err, user) => {
                        if (err) {
                            return reply(err);
                        }
                        if (user) {
                            return reply(Boom.conflict('Email already in use.'));
                        }
                        reply(true);
                    });
                }
            }]
        },
        handler: function (request, reply) {

            const mailer = request.server.plugins.mailer;
            Async.auto({
                user: function (done) {
                    const password = request.payload.profile_pass;
                    const email = request.payload.profile_email;
                    const username = request.payload.profile_email;
                    User.create(username,password, email, done);
                },
                account: ['user', function (results, done) {
                    const data_account = {
                        profile_prenom : request.payload.profile_prenom,
                        profile_nom : request.payload.profile_nom,
                        profile_gender : request.payload.profile_gender,
                        profile_etudiant : request.payload.profile_etudiant,
                        profile_date : request.payload.profile_date,
                        profile_cheveux_1 : request.payload.profile_cheveux_1,
                        profile_cheveux_2 : request.payload.profile_cheveux_2,
                        profile_cheveux_3 : request.payload.profile_cheveux_3,
                        profile_phone : request.payload.profile_phone,
                        profile_np: request.payload.profile_np,
                        profile_localtion: request.payload.profile_localtion,
                        profile_no: request.payload.profile_no,
                        profile_rue: request.payload.profile_rue,
                        profile_code_porte: request.payload.profile_code_porte,
                        profile_animal_de_compagnie: request.payload.profile_animal_de_compagnie,
                        profile_bleue: request.payload.profile_bleue,
                        profile_blanche: request.payload.profile_blanche,
                        profile_visiteur: request.payload.profile_visiteur,
                        profile_privee: request.payload.profile_privee,
                        profile_no_de_place: request.payload.profile_no_de_place,
                        profile_avartar: request.payload.profile_avartar,
                        profile_cover_image: request.payload.profile_cover_image
                    };
                    Account.create(data_account, done);
                }],
                linkUser: ['account', function (results, done) {
                    const id = results.account._id.toString();
                    const update = {
                        $set: {
                            user: {
                                id: results.user._id.toString(),
                                name: results.user.email
                            }
                        }
                    };

                    Account.findByIdAndUpdate(id, update, done);
                }],
                linkAccount: ['account', function (results, done) {

                    const id = results.user._id.toString();
                    const update = {
                        $set: {
                            roles: {
                                account: {
                                    id: results.account._id.toString(),
                                    name: results.account.profile_prenom + ' ' + results.account.profile_nom
                                }
                            }
                        }
                    };

                    User.findByIdAndUpdate(id, update, done);
                }],
                welcome: ['linkUser', 'linkAccount', function (results, done) {

                    const emailOptions = {
                        subject: 'Your ' + Config.get('/projectName') + ' account',
                        to: {
                            name: request.payload.profile_prenom,
                            address: request.payload.profile_email
                        }
                    };
                    const template = 'welcome';

                    mailer.sendEmail(emailOptions, template, request.payload, (err) => {

                        if (err) {
                            console.warn('sending welcome email failed:', err.stack);
                        }
                    });

                    done();
                }],
                session: ['linkUser', 'linkAccount', function (results, done) {

                    Session.create(results.user._id.toString(), done);
                }]
            }, (err, results) => {

                if (err) {
                    return reply(err);
                }

                const user = results.linkAccount;
                const credentials = user.username + ':' + results.session.key;
                const authHeader = 'Basic ' + new Buffer(credentials).toString('base64');
                const result = {
                    user: {
                        _id: user._id,
                        username: user.username,
                        email: user.email,
                        roles: user.roles
                    },
                    session: results.session,
                    authHeader
                };

                request.cookieAuth.set(result);
                reply(result);
            });
        }
    });





    next();
};


exports.register = function (server, options, next) {

    server.dependency(['mailer', 'hapi-mongo-models'], internals.applyRoutes);

    next();
};


exports.register.attributes = {
    name: 'signup'
};
